'use strict';

$(document).on('ready', function () {
  $('[data-background]').each(function (index, elem) {
    if ($(elem).hasClass('circle')) {
      $(elem).css('background', $(elem).data('background'));
    }
  });

  $('[data-color]').each(function (index, elem) {
    $(elem).css('color', $(elem).data('color'));
  });
});
//# sourceMappingURL=application.js.map
