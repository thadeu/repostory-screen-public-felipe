<?php 

$posts = [
	[
		'category' => 'Artigo', 
		'title' => 'teste', 
		'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea amet saepe illo, odio officiis quam quibusdam quo. Praesentium, quae ex? Praesentium optio vitae ipsa, explicabo autem suscipit ipsum? Itaque, at.Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea amet saepe illo, odio officiis quam quibusdam quo. Praesentium, quae ex? Praesentium optio vitae ipsa, explicabo autem suscipit ipsum? Itaque, at.Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea amet saepe illo, odio officiis quam quibusdam quo. Praesentium, quae ex? Praesentium optio vitae ipsa, explicabo autem suscipit ipsum? Itaque, at.', 
		'suject' => 'Doença de Lyme Tocantins Brasil',
		'author' => 'BASTOS, Whisllay Maciel; TAMAYO, César Corranza',
		'publication_at' => 'Mês/Ano Publicação 07/2012',
		'circle-background' => '#e7c83f', 
		'color' => '#e7c83f',
	],
	[
		'category' => 'Artigo', 
		'title' => 'teste', 
		'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea amet saepe illo, odio officiis quam quibusdam quo. Praesentium, quae ex? Praesentium optio vitae ipsa, explicabo autem suscipit ipsum? Itaque, at.', 
		'suject' => 'Doença de Lyme Tocantins Brasil',
		'author' => 'BASTOS, Whisllay Maciel; TAMAYO, César Corranza',
		'publication_at' => 'Mês/Ano Publicação 07/2012',
		'circle-background' => '#f69', 
		'color' => '#e7c83f',
	],
	[
		'category' => 'Artigo', 
		'title' => 'teste', 
		'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea amet saepe illo, odio officiis quam quibusdam quo. Praesentium, quae ex? Praesentium optio vitae ipsa, explicabo autem suscipit ipsum? Itaque, at.', 
		'suject' => 'Doença de Lyme Tocantins Brasil',
		'author' => 'BASTOS, Whisllay Maciel; TAMAYO, César Corranza',
		'publication_at' => 'Mês/Ano Publicação 07/2012',
		'circle-background' => '#900', 
		'color' => '#e7c83f',
	],
]

?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<!-- METAS -->
	<meta charset="UTF-8">

	<!-- CSS -->
	<link rel="stylesheet" href="app/components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="app/assets/stylesheets/application.css">

	<!-- TITLE -->
	<title>frontend-gulp-boirlerplate</title>
</head>
<body>

	<div class="container--main container-fluid">
		<div class="col-md-9">
			<section class="articles">
				<?php foreach($posts as $post) : ?>
					<article class="articles--item">
						<p class="articles--item_category">
							<span class="circle" data-background="<?php echo $post['circle-background']; ?>"></span>
							<?php echo $post['category']; ?>
						</p>
	
						<h1 class="articles--item_title" data-color="<?php echo $post['color']; ?>">
							<?php echo $post['title']; ?>
						</h1>
												
						<p class="articles--item_description"><?php echo $post['description']; ?></p>
						<p class="articles--item_subject"><?php echo $post['suject']; ?></p>
						<p class="articles--item_author"><?php echo $post['author']; ?></p>
						<p class="articles--item_publication_at"><?php echo $post['publication_at']; ?></p>
					</article>
				<?php endforeach; ?>
			</section>
		</div>

		<div class="col-md-3">
			<div class="box-panel">
				<div class="box-panel--title">Categorias</div>
				<div class="box-panel--content">
					<ul class="list-unstyled">
						<li>
							<a href="#">
								<span class="circle" data-background="#e7c83f"></span>
								Artigo (20)
							</a>
						</li>
						<li>
							<a href="#">
								<span class="circle" data-background="#f69"></span>
								Dissertação (10)
							</a>
						</li>
						<li>
							<a href="#">
								<span class="circle" data-background="#900"></span>
								Nota Técnica (4)
							</a>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="box-panel">
				<div class="box-panel--title">Assunto</div>
				<div class="box-panel--content">
					<ul class="list-unstyled">
						<li><a href="#">Doença de Lyme (20)</a></li>
						<li><a href="#">Infecções HIV (15)</a></li>
						<li><a href="#">Encéfalo (5)</a></li>
					</ul>
				</div>
			</div>
			
			<div class="box-panel">
				<div class="box-panel--title">Ano de Publicação</div>
				<div class="box-panel--content">
					<ul class="list-unstyled">
						<li><a href="#">2017 (20)</a></li>
						<li><a href="#">2016 (15)</a></li>
						<li><a href="#">2015 (5)</a></li>
					</ul>
				</div>
			</div>

		</div>
	</div>

  <!-- JS -->
	<script src="app/components/jquery2/jquery.min.js"></script>
	<script src="app/components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="app/assets/javascripts/application.js"></script>
</body>
</html>
