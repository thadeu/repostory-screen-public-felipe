### Projeto .
Projeto Open source para comunidade Front-End/Back-end que utiliza o Gulp como automatizador de tarefas e componentes.
Agora é só rodar o gulp.

Digite: 'gulp watch' e ative a extensão LiveReload do Chrome Browser.
		
### LICENÇA E CRÉDITOS
Criado por Thadeu Esteves Jr.
Email: tadeuu@gmail.com
Blog: http://tadeuesteves.wordpress.com
Site: http://thadeuesteves.com.br
Licença: Livre GPL para uso e distruibuição.
