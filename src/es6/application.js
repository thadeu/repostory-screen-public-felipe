$(document).on('ready', () => {
  $('[data-background]').each((index, elem) => {
    if ($(elem).hasClass('circle')) {
      $(elem).css('background', $(elem).data('background'))
    }
  })
  
  $('[data-color]').each((index, elem) => {
    $(elem).css('color', $(elem).data('color'))
  })
})